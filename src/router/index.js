import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

function lazyLoad(view) {
  return () => import(`@/${view}.vue`);
}

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "login",
      component: lazyLoad("views/Login")
    },
    {
      path: "/expired-session",
      name: "expired-session",
      component: lazyLoad("views/ExpiredSession")
    },
    {
      path: "/platform-admin",
      name: "admin-login",
      component: lazyLoad("views/AdminLogin")
    },
    {
      path: "/platform-panel/dashboard",
      name: "admin-dashboard",
      component: lazyLoad("views/AdminPanelAdmins"),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/platform-panel/riders",
      name: "admin-riders",
      component: lazyLoad("views/AdminPanelRiders"),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/hospitalAdmin/resetPassword/:role",
      name: "reset-password",
      component: lazyLoad("views/ResetPasswordPage"),
      meta: {
        guest: true
      }
    },
    {
      path: "/rider",
      name: "rider-dashboard",
      component: lazyLoad("views/RiderDashboard"),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/admin",
      component: lazyLoad("views/AdminDashboard"),
      children: [
        {
          path: "",
          name: "alerts",
          component: lazyLoad("components/New"),
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "accepted",
          name: "accepted",
          component: lazyLoad("components/Accepted"),
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "rejected",
          name: "rejected",
          component: lazyLoad("components/Rejected"),
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "ignored",
          name: "viewed",
          component: lazyLoad("components/Ignored"),
          meta: {
            requiresAuth: true
          }
        },
        {
          path: "stats",
          name: "admin-stats",
          component: lazyLoad("components/Statistics"),
          meta: {
            requiresAuth: true
          }
        }
      ]
    },
    {
      path: "*",
      component: lazyLoad("views/PageNotFound")
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem("token") == null) {
      next("/");
    } else {
      next();
      return;
    }
  } else {
    next();
    return;
  }
});

export default router;
