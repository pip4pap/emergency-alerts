import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import "./registerServiceWorker";
import vuetify from "./plugins/vuetify.js";
import Axios from "axios";
import axiosInstance from "./store/axios_setup";
import store from "./store/store";
import * as VueGoogleMaps from "vue2-google-maps";

Vue.prototype.$http = Axios;
const token = localStorage.getItem("token");
if (token) {
  Vue.prototype.$http.defaults.headers.common[
    "Authorization"
  ] = `bearer ${token}`;
}

axiosInstance.interceptors.response.use(
  response => {
    if (response.status === 200 || response.status === 201) {
      return Promise.resolve(response);
    } else {
      return Promise.reject(response);
    }
  },
  error => {
    if (
      error.response.data.message ==
      "Invalid token, Please log in to gain access"
    ) {
      router.push("/expired-session");
    } else {
      return Promise.reject(error);
    }
  }
);

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyA92H6y5BIZxBNT_Ch3aCMXz5qSc4xYvVU"
  }
});
Vue.prototype.$http = Axios;
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
