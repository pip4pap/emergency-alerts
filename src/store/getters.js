export const getters = {
  acceptedAlerts: state => {
    return state.statAlerts.filter(alert => {
      return alert[state.joinTable].status === "accepted";
    });
  },
  rejectedAlerts: state => {
    return state.statAlerts.filter(alert => {
      return alert[state.joinTable].status === "rejected";
    });
  },
  ignoredAlerts: state => {
    return state.statAlerts.filter(alert => {
      return alert[state.joinTable].status === "viewed";
    });
  }
};
