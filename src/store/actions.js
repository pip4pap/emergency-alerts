import axiosInstance from "./axios_setup";

export const actions = {
  async Login({ commit }, data) {
    commit("isLoading", true);
    let url = "";
    let endpoints = {
      police: "policeAdmin",
      hospital: "hospitalAdmin",
      rider: "rider"
    };
    if (data.isAdmin) {
      url = "/admin/login";
    } else {
      url = `/${endpoints[data.tag]}/login`;
    }
    await axiosInstance
      .post(url, data.credentials)
      .then(res => {
        localStorage.setItem("token", res.data.token);
        localStorage.setItem("role", res.data.data.role);
        commit("setUser", res.data.data);
        commit("isLoading", false);
      })
      .catch(error => {
        commit("loginError", error.response.data.message);
        commit("isLoading", false);
      });
  },
  async fetchLoggedInAdmin({ commit }, data) {
    let accessToken = localStorage.getItem("token");
    axiosInstance.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${accessToken}`;
    let endpointBase = data === "PoliceAdmin" ? "policeAdmin" : "hospitalAdmin";
    await axiosInstance
      .get(`/${endpointBase}/me`)
      .then(res => {
        commit("setUser", res.data.data);
      })
      .catch(error => {
        commit("loginError", error.response.data.message);
      });
  },
  async FetchLoggedInRider({ commit }) {
    let accessToken = localStorage.getItem("token");
    axiosInstance.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${accessToken}`;
    await axiosInstance
      .get("/rider/me")
      .then(res => {
        commit("setUser", res.data.data);
      })
      .catch(error => {
        commit("loginError", error.response.data.message);
      });
  },
  async Signup({ commit }, data) {
    commit("isLoading", true);
    let endpointBase = data.tag === "police" ? "policeAdmin" : "hospitalAdmin";
    await axiosInstance
      .post(`/${endpointBase}/signup`, data.details)
      .then(res => {
        localStorage.setItem("token", res.data.token);
        localStorage.setItem("role", res.data.data.role);
        commit("setUser", res.data.data);
        commit("isLoading", false);
      })
      .catch(error => {
        commit("signupError", error.response.data.message);
        commit("isLoading", false);
      });
  },
  async sendPasswordResetLink({ commit }, data) {
    commit("isLoading", true);
    await axiosInstance
      .patch("/hospitalAdmin/forgotPassword", data)
      .then(res => {
        commit("setforgotPasswordMessage", {
          email: data.email,
          status: res.data.status
        });
        commit("isLoading", false);
      })
      .catch(error => {
        commit("setforgotPasswordMessage", {
          status: error.response.data.status,
          message: error.response.data.message
        });
        commit("isLoading", false);
      });
  },
  async FetchAllAdmins({ commit }, data) {
    commit("tableLoading", true);
    let accessToken = localStorage.getItem("token");
    axiosInstance.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${accessToken}`;
    let endpointBase = data === "police" ? "policeAdmin" : "hospitalAdmin";
    await axiosInstance
      .get(`/${endpointBase}`)
      .then(res => {
        commit("setAdminsList", {
          tag: data,
          payload: res.data.data
        });
        commit("tableLoading", false);
      })
      .catch(error => {
        commit("tableLoading", false);
        commit("fetchAllAdminsError", error.response.data.message);
      });
  },
  async FetchAllRiders({ commit }) {
    commit("tableLoading", true);
    let accessToken = localStorage.getItem("token");
    axiosInstance.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${accessToken}`;
    await axiosInstance
      .get("/rider")
      .then(res => {
        commit("setRidersList", res.data.data);
        commit("tableLoading", false);
      })
      .catch(error => {
        commit("tableLoading", false);
        commit("fetchAllAdminsError", error.response.data.message);
      });
  },
  async FetchAlerts({ commit }, data) {
    commit("isoverlayLoading", true);
    let accessToken = localStorage.getItem("token");
    axiosInstance.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${accessToken}`;
    let endpointBase =
      data.tag === "PoliceAdmin" ? "policeAdmin" : "hospitalAdmin";
    let joinTable =
      data.tag === "PoliceAdmin" ? "PoliceCrash" : "HospitalCrash";
    await axiosInstance
      .get(`/${endpointBase}/crashes`)
      .then(res => {
        commit("setAlerts", res.data.data);
        commit("setJoinTable", joinTable);
        let alerts = res.data.data;
        if (data.section) {
          let filteredAlerts = alerts.filter(alert => {
            return alert[joinTable].status === data.section;
          });
          commit("setCrashes", filteredAlerts);
          commit("isoverlayLoading", false);
        }
      })
      .catch(error => {
        commit("isoverlayLoading", false);
        commit("fetchCrashError", error.response.data.message);
      });
  },
  async FetchEmergencyPlaceDetails({ commit }, data) {
    commit("isoverlayLoading", true);
    commit("setJoinTable", data.joinTable);
    let accessToken = localStorage.getItem("token");
    axiosInstance.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${accessToken}`;
    let endpointBase =
      data.tag === "PoliceAdmin" ? "policeAdmin" : "hospitalAdmin";
    let emergencyPlace = data.tag === "PoliceAdmin" ? "police" : "hospital";
    await axiosInstance
      .get(`/${endpointBase}/${emergencyPlace}`)
      .then(res => {
        commit("setEmergencyPlaceDetails", {
          tag: emergencyPlace,
          data: res.data.data
        });
        commit("isoverlayLoading", false);
      })
      .catch(error => {
        commit("isoverlayLoading", false);
        commit("loginError", error.response.data.message);
      });
  },
  async ApproveAdmin({ commit }, data) {
    commit("isLoading", true);
    let accessToken = localStorage.getItem("token");
    axiosInstance.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${accessToken}`;
    let endpointBase = data.tag === "police" ? "policeAdmin" : "hospitalAdmin";
    await axiosInstance.patch(`/${endpointBase}/approve`, data.id).then(res => {
      commit("adminActionSuccess", `${res.data.status}. ${res.data.message}`);
      commit("isLoading", false);
    });
  },
  async DeclineAdmin({ commit }, data) {
    commit("isLoading", true);
    let accessToken = localStorage.getItem("token");
    axiosInstance.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${accessToken}`;
    let endpointBase = data.tag === "police" ? "policeAdmin" : "hospitalAdmin";
    await axiosInstance.patch(`/${endpointBase}/deny`, data.id).then(res => {
      commit("adminActionSuccess", `${res.data.status}. ${res.data.message}`);
      commit("isLoading", false);
    });
  },
  async RequestEmergencyPlaceAttachment({ commit }, data) {
    commit("isLoading", true);
    let accessToken = localStorage.getItem("token");
    axiosInstance.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${accessToken}`;
    let endpointBase = data.tag === "police" ? "policeAdmin" : "hospitalAdmin";
    await axiosInstance
      .patch(`/${endpointBase}/${data.tag}`, data.details)
      .then(res => {
        commit("requestAttachmentSuccess", res.data.status);
        commit("isLoading", false);
      });
  },
  async RegisterRider({ commit }, data) {
    commit("isLoading", true);
    let accessToken = localStorage.getItem("token");
    axiosInstance.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${accessToken}`;
    await axiosInstance
      .post("/rider", data)
      .then(res => {
        if (res.data.status === "success") {
          commit("openRiderRegistration", false);
        }
      })
      .catch(error => {
        commit("setRegisterRiderError", error.response.data.message);
      });
  },
  SearchTerm({ commit }, data) {
    commit("searchAlerts", data);
  },
  StoreAlerts({ commit }, data) {
    commit("getPendingAlerts", data);
  },
  SwitchAdminsTable({ commit }, data) {
    commit("switchAdminsTable", data);
  },
  TriggerRiderRegistration({ commit }, data) {
    commit("openRiderRegistration", data);
  },
  TriggerRiderDetails({ commit }, data) {
    commit("openRiderDetails", data);
  },
  SetRiderDetails({ commit }, data) {
    commit("openRiderDetails", data.toggle);
    commit("setRiderDetails", data.rider);
  }
};
