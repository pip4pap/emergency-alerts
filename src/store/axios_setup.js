import axios from "axios";
const API_URL = "https://emergency-alerts.herokuapp.com/api";
// Comment line above and uncomment line below to connect to local server
// const API_URL = "http://localhost:3000/api";

let settings = {
  baseURL: API_URL
};
export default axios.create(settings);
