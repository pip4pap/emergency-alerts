export const mutations = {
  acceptAlert(state, data) {
    data.status = "Accepted";
    state.acceptedAlerts.unshift(data);
  },
  searchAlerts(state, data) {
    state.searchTerm = data;
  },
  rejectAlert(state, data) {
    data.status = "Rejected";
    state.rejectedAlerts.unshift(data);
  },
  ignoreAlert(state, data) {
    data.status = "Ignored";
    state.ignoredAlerts.unshift(data);
  },
  requestAttachmentSuccess(state, data) {
    state.requestAttachmentSuccess = data;
  },
  isLoading(state, data) {
    state.isLoading = data;
  },
  isoverlayLoading(state, data) {
    state.overlayLoader = data;
  },
  adminActionSuccess(state, data) {
    state.adminActionFeedback = data;
  },
  loginError(state, data) {
    state.loginError = data;
    state.isLoggedIn = false;
  },
  setUser(state, data) {
    state.user = data;
    state.isLoggedIn = true;
  },
  tableLoading(state, data) {
    state.tableLoading = data;
  },
  setEmergencyPlaceDetails(state, data) {
    state.emergencyPlaceID = data.data.ID;
    state.emergencyPlace = data.data[`${data.tag}Name`];
    state.emergencyPlaceLocation = {
      latitude: data.data[`${data.tag}Latitude`],
      longitude: data.data[`${data.tag}Longitude`]
    };
    state.emergencyPlaceGoogleID = data.data[`${data.tag}PlaceID`];
  },
  setCrashes(state, data) {
    state.APIalerts = data;
  },
  setAlerts(state, data) {
    state.statAlerts = data;
  },
  setJoinTable(state, data) {
    state.joinTable = data;
  },
  fetchCrashError(state, data) {
    state.fetchCrashError = data;
  },
  setAdminsList(state, data) {
    if (data.tag === "police") {
      state.policeAdmins = data.payload;
    } else {
      state.hospitalAdmins = data.payload;
    }
  },
  openRiderRegistration(state, data) {
    state.riderRegistrationTrigger = data;
  },
  openRiderDetails(state, data) {
    state.riderDetailsTrigger = data;
  },
  setRiderDetails(state, data) {
    state.riderDetails = data;
  },
  setRegisterRiderError(state, data) {
    state.registerRiderError = data;
  },
  setRidersList(state, data) {
    state.riders = data;
  },
  switchAdminsTable(state, data) {
    state.adminRoleButtons = data;
  },
  fetchAllAdminsError(state, data) {
    state.fetchAllAdminsError = data;
  },
  getPendingAlerts(state, data) {
    state.alerts = data;
  },
  alertsError(state, data) {
    state.getAlertsError = data;
  },
  setHospitalList(state, data) {
    state.hospitals = data;
  },
  signupError(state, data) {
    state.signupError = data;
  },
  setforgotPasswordMessage(state, data) {
    if (data.status === "success") {
      state.forgotPasswordMessage = `Success. A password reset link has been sent to ${data.email}`;
    } else {
      state.forgotPasswordMessage = `Failed. ${data.message}`;
    }
  }
};
