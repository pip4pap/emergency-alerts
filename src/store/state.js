export const state = {
  forgotPasswordMessage: "",
  alerts: [],
  statAlerts: [],
  APIalerts: [],
  hospitalAdmins: [],
  policeAdmins: [],
  joinTable: "",
  requestAttachmentSuccess: "",
  adminRoleButtons: "",
  riders: [],
  riderDetails: {},
  adminActionFeedback: "",
  riderRegistrationTrigger: false,
  riderDetailsTrigger: false,
  tableLoading: false,
  overlayLoader: false,
  isLoading: null,
  loginError: null,
  registerRiderError: null,
  getAlertsError: null,
  fetchCrashError: null,
  fetchAllAdminsError: null,
  isLoggedIn: false,
  emergencyPlaceID: "",
  emergencyPlace: "",
  emergencyPlaceGoogleID: "",
  hospitals: [],
  user: {},
  emergencyPlaceLocation: null,
  searchTerm: null,
  signupError: null
};
