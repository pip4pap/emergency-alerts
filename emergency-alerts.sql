-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: localhost    Database: Crashes
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Admin_Users`
--

DROP TABLE IF EXISTS `Admin_Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Admin_Users` (
  `Admin_ID` int NOT NULL AUTO_INCREMENT,
  `Username` varchar(45) NOT NULL,
  `Password` varchar(45) NOT NULL,
  `Hospital_ID` int NOT NULL,
  PRIMARY KEY (`Admin_ID`),
  KEY `Hospital_ID_idx` (`Hospital_ID`),
  CONSTRAINT `Admin_Hospital_ID` FOREIGN KEY (`Hospital_ID`) REFERENCES `Hospital` (`Hospital_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Admin_Users`
--

LOCK TABLES `Admin_Users` WRITE;
/*!40000 ALTER TABLE `Admin_Users` DISABLE KEYS */;
INSERT INTO `Admin_Users` VALUES (10105,'admin@mak.hopsital.ac.ug','tree.964.great',330);
/*!40000 ALTER TABLE `Admin_Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Crash`
--

DROP TABLE IF EXISTS `Crash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Crash` (
  `CrashID` int NOT NULL AUTO_INCREMENT,
  `Crash_latitude` varchar(45) NOT NULL,
  `Crash_longitude` varchar(45) NOT NULL,
  `Timestamp` varchar(45) NOT NULL,
  `Rider_ID` int NOT NULL,
  `Hospital_ID` int NOT NULL,
  `Status` varchar(10) NOT NULL,
  PRIMARY KEY (`CrashID`),
  KEY `Rider_ID_idx` (`Rider_ID`),
  KEY `Hospital_ID_idx` (`Hospital_ID`),
  CONSTRAINT `Hospital_ID` FOREIGN KEY (`Hospital_ID`) REFERENCES `Hospital` (`Hospital_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Rider_ID` FOREIGN KEY (`Rider_ID`) REFERENCES `Rider` (`Rider_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Crash`
--

LOCK TABLES `Crash` WRITE;
/*!40000 ALTER TABLE `Crash` DISABLE KEYS */;
INSERT INTO `Crash` VALUES (116,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending'),(117,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending'),(118,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending'),(119,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending'),(120,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending'),(121,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending'),(122,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending'),(123,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending'),(124,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending'),(125,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending'),(126,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending'),(127,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending'),(128,'0.323172','32.620802','2020-04-28 16:04:48',12012,337,'Pending');
/*!40000 ALTER TABLE `Crash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Hospital`
--

DROP TABLE IF EXISTS `Hospital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Hospital` (
  `Hospital_ID` int NOT NULL AUTO_INCREMENT,
  `Hospital_Name` varchar(200) NOT NULL,
  `Hospital_latitude` varchar(45) NOT NULL,
  `Hospital_longitude` varchar(45) NOT NULL,
  PRIMARY KEY (`Hospital_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=338 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Hospital`
--

LOCK TABLES `Hospital` WRITE;
/*!40000 ALTER TABLE `Hospital` DISABLE KEYS */;
INSERT INTO `Hospital` VALUES (330,'Makerere University Hospital','0.3279485','32.5706458'),(337,'Middle East Hospital & Diagnostic Centre','0.3189554000000001','32.6229319');
/*!40000 ALTER TABLE `Hospital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Police`
--

DROP TABLE IF EXISTS `Police`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Police` (
  `Police_ID` int NOT NULL AUTO_INCREMENT,
  `Police_Name` varchar(45) NOT NULL,
  `Police_Location` varchar(45) NOT NULL,
  PRIMARY KEY (`Police_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Police`
--

LOCK TABLES `Police` WRITE;
/*!40000 ALTER TABLE `Police` DISABLE KEYS */;
/*!40000 ALTER TABLE `Police` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Rider`
--

DROP TABLE IF EXISTS `Rider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Rider` (
  `Rider_ID` int NOT NULL AUTO_INCREMENT,
  `Rider_Name` varchar(45) NOT NULL,
  `Next_Of_kin_Name` varchar(45) NOT NULL,
  `Next_Of_kin_Contact` varchar(45) NOT NULL,
  PRIMARY KEY (`Rider_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12013 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Rider`
--

LOCK TABLES `Rider` WRITE;
/*!40000 ALTER TABLE `Rider` DISABLE KEYS */;
INSERT INTO `Rider` VALUES (12011,'Muwanguzi Gideon','Nassiwa Primrose','0778234732'),(12012,'Twinamastiko Jonah','Assimwe Rose','0704273823');
/*!40000 ALTER TABLE `Rider` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-04 11:30:06
